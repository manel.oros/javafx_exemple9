/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package prac1.utils;

import java.nio.file.*;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

/**
 * Utilitats varies per a interactuar amb fixters MP3 del disc
 * 
 * @author manel
 */
public class FileUtils {
    
    /***
     * 
     * @param url
     * 
     * @return Si la URL és de tipus Windows, llavors elimina el nom de la unitat
     * 
     */
    public static String normalizeURLFormat(String url)
    {
        String ret = "";
        
        if (IdentificaOS.getOS() == IdentificaOS.TipusOS.WIN)
            ret = url.replace("[A-Z]{1}:", "");
        
        return ret;
    }
    
    /***
     * Retorna la ruta al MP3 de prova dels recursos.
     * La ruta es torna en format URL. P.ex: 
     * 
     *  Si s'executa sobre Windows, ho retorna en el format: file:/C:/Users/manel/.../target/classes/sounds/test_sound.mp3
     *  Si s'executa sobre Linux, ho retorna en el format: file:/home/manel/.../target/classes/sounds/test_sound.mp3
     * 
     *  https://en.wikipedia.org/wiki/File_URI_scheme
     * 
     * @param instance instància des de la qual es crida (this)
     * @return 
     */
    public static String getTestMP3(Object instance)
    {
        return instance.getClass().getClassLoader().getResource("sounds/test_sound.mp3").toString();
    }
    
    /***
     * Retorna una icona dels recursos
     * 
     * @param instance
     * @param nom
     * @return 
     */
    public static String getIcona(Object instance, String nom)
    {
        return instance.getClass().getClassLoader().getResource("icons/" + nom).toString();
    }
    
    /***
     * Permet seleccionar un fitxer MP3 d'una unitat del disc
     * 
     * @return 
     */
    public static Path getMP3Fromfile()
    {
        Path ret = null;
        
        Stage stage1 = new Stage();
        
        FileChooser filechooser1 = new FileChooser();
        
        filechooser1.setTitle("Seleccionar fixter MP3");
        Path fitxerMP3 = filechooser1.showOpenDialog(stage1).toPath();
        
        if (fitxerMP3 != null)
            ret = fitxerMP3;
        
        return ret;
    }
    
}
