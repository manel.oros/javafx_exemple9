/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package prac1.controllers;


import java.net.URL;
import java.util.ResourceBundle;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.media.Media;
import javafx.scene.media.MediaException;
import javafx.scene.media.MediaPlayer;
import prac1.utils.FileUtils;

/**
 * FXML Controller class
 *
 * @author manel
 */
public class MainScreenController implements Initializable {
    
    Media media = null;
    
    MediaPlayer player = null;
    
    
    ImageView imgPlay, imgStop;
    
    @FXML
    private Label lbl1;
    
    @FXML
    private Button btn_test;
    
    @FXML
    void on_botTestClic(ActionEvent event) {
        
        // Si apreten botó mentre el reproductor encara està en reproducció, el parem
        if (player.getStatus() != MediaPlayer.Status.PLAYING)
            player.play();
        else
            player.stop();
    }
  
    /***
     * Inicialitza el controlador
     * 
     * @param url
     * @param rb 
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        // inicialitzem imatges icona
        imgPlay = new ImageView(FileUtils.getIcona(this,"play.png"));
        imgPlay.setFitHeight(66);
        imgPlay.setFitWidth(66);
        imgStop = new ImageView(FileUtils.getIcona(this,"stop.png"));
        imgStop.setFitHeight(66);
        imgStop.setFitWidth(66);

        btn_test.setGraphic(imgPlay);
        
       //inicialment tenim desactivat el botó de test
       this.btn_test.setDisable(true); 
        
       //inicialitzem reproductor amb mp3 de prova
       String path  = FileUtils.getTestMP3(this);
       openMedia(path);

        // un cop el reproductor està preparat, podem activar el botó per a procedir
        player.setOnReady(() -> {

            this.btn_test.setDisable(false);

        });
       
       // mostrem missatge
       lbl1.setText("Ajusta el volum d'audio i prem play");
       
       // esdeveniment que para el reproductor quan finalitza la reproducció
       player.setOnEndOfMedia(() -> {
           player.stop();
       });
       
       // esdeveniment que si el reproductor esta en playing, posem la icona del stop
       player.setOnPlaying(() -> this.setButtonIcon(false));
       
       //esdeveniment que si el reproductor està preparat, posem el de play
       player.setOnStopped(() -> this.setButtonIcon(true));
       
       // esdeveniment que es desencadena quan canvia l'estatus del player
       player.statusProperty().addListener((observable, oldValue, newValue) -> 
       {
          
           ObservableValue o = (ObservableValue)observable;
           MediaPlayer.Status status = (MediaPlayer.Status)o.getValue();
           System.out.println("Player status: " + status );
           
           // same as: System.out.println("Player status: " + newValue);
       });
        
    }
    
    /***
     * Inicialitza el reproductor amb un fitxer MP3
     * 
     * El format ha de ser de tipus URL
     * 
     * 
     */
    private void openMedia(String path)
    {
        try{
            
            // actuaslitzem el recurs MP3
            this.media = new Media(path);

            // inicialitzem el reproductor
            this.player = new MediaPlayer(media);
            
        } catch (MediaException e) {
            
            System.out.println("ERROR obrint fitxer demo: " + path + ":" + e.toString());
        }
    }
    
    /***
     * Estableix l'icona del botó a play o stop
     * 
     * @param play 
     */
    private void setButtonIcon(boolean play)
    {   
        if (play)
             btn_test.setGraphic(imgPlay);
        else 
             btn_test.setGraphic(imgStop);
    }
}
